package com.classpath.shipment.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppHealthEndpoint implements HealthIndicator {

	@Override
	public Health health() {
		//db query
		//kafka server
		return Health.up().withDetail("DB-service", "DB service is up").build();
	}
}

@Configuration
class KafkaEndpoint implements HealthIndicator{
	@Override
	public Health health() {
		//db query
		//kafka server
		return Health.up().withDetail("Kafka-service", "Kafkaservice is up").build();
	}
}
