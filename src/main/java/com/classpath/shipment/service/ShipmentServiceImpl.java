package com.classpath.shipment.service;

import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.classpath.shipment.model.Shipment;
import com.classpath.shipment.repository.ShipmentRepository;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ShipmentServiceImpl implements ShipmentService {
	
	private ShipmentRepository shipmentRepository;
	
	@Override
	public Shipment save(Shipment shipment) {
		return this.shipmentRepository.save(shipment);
	}

	@Override
	public Page<Shipment> fetchAll(Pageable pageable) {
		return this.shipmentRepository.findAll(pageable);
	}
	
	@Override
	public Shipment updateShipmentById(long shipmentId, Shipment shipment) {
		if (this.shipmentRepository.existsById(shipmentId)) {
			Shipment shipmentDto = this.shipmentRepository.getOne(shipmentId);
			shipmentDto.setId(shipmentId);
			shipmentDto.setDestination(shipment.getDestination());
			shipmentDto.setStatus(shipment.getStatus());
			shipmentDto.setDeliveryDate(shipment.getDeliveryDate());
			return this.shipmentRepository.save(shipmentDto);
		}
		return shipment;
	}

	@Override
	public Shipment findById(long id) {
		return this.shipmentRepository
					.findById(id)
					.orElseThrow(() -> new IllegalArgumentException("Invalid shipment id"));
	}
	
	@Override
	public void deleteById(long shipmentId) {
		this.shipmentRepository.deleteById(shipmentId);
	}
}
